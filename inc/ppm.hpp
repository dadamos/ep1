#ifndef PPM_HPP
#define PPM_HPP

#include "imagem.hpp"

class Ppm : public Imagem {
	private:
		//atributos
		string mensagem;
	public:
		//métodos

		Ppm();
		~Ppm();

		void decifraPpm(ifstream& arquivo, int largura, int altura, string saida);
		virtual void decifraPgm(ifstream& arquivo, ofstream& saida){};
};

#endif