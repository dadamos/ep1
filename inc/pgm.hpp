#ifndef PGM_HPP
#define PGM_HPP

#include "imagem.hpp"

class Pgm : public Imagem {
	private: 
		//atributos
		string mensagem;
	public:
		//métodos

		Pgm();
		~Pgm();

		void decifraPgm(ifstream& arquivo, ofstream& saida);
		virtual void decifraPpm(ifstream& arquivo, int largura, int altura, string saida) {};
};

#endif