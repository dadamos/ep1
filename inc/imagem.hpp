#ifndef IMAGEM_HPP
#define IMAGEM_HPP

#include <iostream>
#include <string>
#include <fstream>

using namespace std;

class Imagem {
	// Atributos
	private:
		string tipo;
		string nome;
		string num_magico;
		int posicao_inicial;
		int largura, altura;
		string nivel_cor_max;
		string arquivo_saida;
	public:
	// Métodos

		Imagem();	// Construtor
		~Imagem();	// Destrutor

		void setTipo(string tipo);
		string getTipo();

		void setNome(string nome);
		string getNome();

		void setNumMagico(string num_magico);
		string getNumMagico();

		void setPosicaoInicial(int posicao_inicial);
		int getPosicaoInicial();

		void setTamanho(int largura, int altura);
		int getLargura();
		int getAltura();

		void setNivelCorMax(string nivel_cor_max);
		string getNivelCorMax();

		void setArquivoSaida(string arquivo_saida);
		string getArquivoSaida();

		virtual void decifraPgm(ifstream& arquivo, ofstream& saida) = 0;
		virtual void decifraPpm(ifstream& arquivo, int largura, int altura, string saida) = 0;
};

#endif