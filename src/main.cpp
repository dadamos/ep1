#include <sstream>
#include "pgm.hpp"
#include "ppm.hpp"

using namespace std;

int main(int argc, char ** argv) {
	string tipo, caminho, linha, comentario, caminho_saida;
	stringstream ss, tamanho;
	ifstream arquivo;
	ofstream saida;
	int posicao_inicial, largura, altura;

	//lê tipo do arquivo, só aceita pgm ou ppm
	do {
		cout << "Tipo da imagem (pgm ou ppm): ";
		cin >> tipo;	
	} while (tipo != "pgm" && tipo != "ppm");

	//cria o objeto imagem de acordo com o tipo
	Imagem *imagem;
	if (tipo == "pgm")
		imagem = new Pgm();
	else
		imagem = new Ppm();

	//guarda o tipo
	imagem->setTipo(tipo);

	//lê o nome do arquivo da imagem
	string nome;
    cout << "Nome da imagem (sem a extensão): ";
    cin >> nome;
    
    //guarda o nome
    imagem->setNome(nome);

    //caminho pra imagem
	caminho = "src/imagens/" + imagem->getNome() + '.' + imagem->getTipo();

    //abre a imagem e verifica se abriu
	arquivo.open(caminho.c_str(), fstream::in | fstream::binary);
	if(!arquivo.is_open()) {
		cerr << "Nao foi possível abrir a imagem!" << endl;
        return 0;
	}

    //pega o número mágico do cabeçalho da imagem
    getline(arquivo, linha);
    imagem->setNumMagico(linha);

    //se for pgm, pega a posição inicial do comentário
    getline(arquivo, linha);
    if (imagem->getTipo() == "pgm") {
    	ss << linha;
    	ss >> comentario >> posicao_inicial;
    	imagem->setPosicaoInicial(posicao_inicial);
    }

    //pega a largura e altura do cabeçalho da imagem
	getline(arquivo, linha);
	tamanho << linha;
	tamanho >> largura >> altura;
	imagem->setTamanho(largura, altura);

	//pega o nível máximo de cor do cabeçalho da imagem
	getline(arquivo, linha);
	imagem->setNivelCorMax(linha);

	//lê o nome do arquivo de saída
	string nome_saida;
    cout << "Nome do arquivo de saída (sem a extensão): ";
    cin >> nome_saida;
    
    //guarda o nome do arquivo de saída
    imagem->setArquivoSaida(nome_saida);

	//decifra imagem
	if (imagem->getTipo() == "pgm") {
		caminho_saida = "saidas/" + imagem->getArquivoSaida() + ".txt";
		//abre o arquivo de saída e e verifica se abriu
		saida.open(caminho_saida.c_str(), fstream::out | fstream::binary);
		if(!saida.is_open()) {
			cerr << "Nao foi possível abrir o arquivo de saida!" << endl;
	        return 0;
		}
		//se for pgm, posiciona o cursor no  arquivo até a posição inicial da mensagem
		arquivo.seekg(imagem->getPosicaoInicial() + arquivo.tellg() + 8, arquivo.beg);
		//soma 8 para pular o primeira caracter (na imagem da Lena, o primeiro caracter da mensagem é # e ao verificar o fim da mensagem escondida no decifraPgm pelo # deixaria a saida em branco)
		imagem->decifraPgm(arquivo, saida);
		saida.close();
	} else {
		imagem->decifraPpm(arquivo, imagem->getLargura(), imagem->getAltura(), imagem->getArquivoSaida());
	}
	cout << "Feito!" << endl;

	arquivo.close();

	// delete(imagem);

	return 0;
}