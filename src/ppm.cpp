#include "ppm.hpp"
#include <cstdlib>

Ppm::Ppm() {
	
}

void Ppm::decifraPpm(ifstream& arquivo, int largura, int altura, string saida) {
	int linha, coluna, coluna_p;
	char pixel;
	unsigned char pixels[altura][largura][3];

	//pega os pixels
	for(linha = 0; linha < altura; linha++){
		for(coluna = 0; coluna < largura; coluna++){
			for(coluna_p = 0; coluna_p < 3; coluna_p++){
				arquivo.get(pixel);
				pixels[linha][coluna][coluna_p] = (unsigned char)pixel;
			}
		}
	}

	//filtros
	unsigned char filtro;
	for(linha = 0; linha < altura; linha++){
		for(coluna = 0; coluna < largura; coluna++){
			filtro = pixels[linha][coluna][0];
			pixels[linha][coluna][1] = filtro;
			pixels[linha][coluna][2] = filtro;
		}
	}

	//abre o arquivo de saída e e verifica se abriu
	FILE * imagem_saida;
	if((imagem_saida = fopen(("saidas/" + saida + ".ppm").c_str(), "wb")) == NULL){
		cerr << "Nao foi possível abrir o arquivo de saida!" << endl;
	}

	//escreve cabeçalho no arquivo de saída
	fputs("P6", imagem_saida);
	fputs("#segredo revelado\n", imagem_saida);
	fprintf(imagem_saida, "%d %d\n", largura, altura);
	fprintf(imagem_saida, "%d\n", 255);

	//escreve os pixels filtrados no arquivo de saída
	for(linha = 0; linha < altura; linha++){
		for(coluna=0; coluna < largura; coluna++){
			for(coluna_p = 0; coluna_p < 3; coluna_p++){
				fputc(pixels[linha][coluna][coluna_p], imagem_saida);
			}
		}
	}
	fclose(imagem_saida);
}