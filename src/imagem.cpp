#include "imagem.hpp"

using namespace std;

Imagem::Imagem() {
	
}
Imagem::~Imagem() {

}

void Imagem::setTipo(string tipo) {
	this->tipo = tipo;
}
string Imagem::getTipo() {
	return tipo;
}

void Imagem::setNome(string nome) {
	this->nome = nome;
}
string Imagem::getNome() {
	return nome;
}

void Imagem::setNumMagico(string num_magico) {
	this->num_magico = num_magico;
}
string Imagem::getNumMagico() {
	return num_magico;
}

void Imagem::setPosicaoInicial(int posicao_inicial) {
	this->posicao_inicial = posicao_inicial;
}
int Imagem::getPosicaoInicial() {
	return posicao_inicial;
}

void Imagem::setTamanho(int largura, int altura) {
	this->largura = largura;
	this->altura = altura;
}
int Imagem::getLargura() {
	return largura;
}
int Imagem::getAltura() {
	return altura;
}

void Imagem::setNivelCorMax(string nivel_cor_max) {
	this->nivel_cor_max = nivel_cor_max;
}
string Imagem::getNivelCorMax() {
	return nivel_cor_max;
}

void Imagem::setArquivoSaida(string arquivo_saida) {
	this->arquivo_saida = arquivo_saida;
}
string Imagem::getArquivoSaida() {
	return arquivo_saida;
}