#include "pgm.hpp"

Pgm::Pgm() {
	
}

void Pgm::decifraPgm(ifstream& arquivo, ofstream& saida) {
	char byte_arquivo, caracter_extraido, final_msg;

	caracter_extraido = 0;
	final_msg = 0;
	int i = 0;
	while (arquivo.get(byte_arquivo) && final_msg != '#') {
		caracter_extraido <<= 1;
		caracter_extraido |= (byte_arquivo & 0x01);
		i++;
		if (i == 8) {
			// cout << caracter_extraido;
			saida << caracter_extraido;
			final_msg = caracter_extraido;
			caracter_extraido = 0;
			i = 0;
		}
	}

}