DESCRIÇÃO

Encontra mensagens escondidas em imagens pgm e ppm.

USO 

Colocar as imagens para serem decifradas no diretório 'src/imagens'. Algumas imagens já se encontram lá para teste.

Para compilar:
make

Para executar:
make run

Ao executar, o programa vai pedir o tipo da imagem que será decifrada. Os tipos aceitos são pgm ou ppm. Enquanto não for digitado um dos tipos o programa continuará pedindo o tipo da imagem.

Depois será pedido o tipo da imagem sem a extensão. Se a imagem não existir na pasta ou não for possível abrí-la, o programa irá avisar e interromper a execução.

Abrindo a imagem corretamente, o programa pedirá o nome do arquivo de saída que será criado. O program cria com o nome informado: um .txt caso a imagem aberta seja .pgm, ou cria um .ppm caso a imagem seja .ppm. Se não for possível criar o arquivo de saída, o programa irá avisar.

Após decifrar a imagem e salvar a mensagem o programa informa "Feito!" e termina.

As saídas sempre são gravadas no diretório 'saidas'.